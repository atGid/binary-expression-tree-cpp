#include <iostream>
#include <string>
#include "stack.h"
#include <cstdlib> //atoi
#include <sstream> //String stream
using namespace std;

string rpnInput();
bool isOperator(char i);
void postfix(string &expression, stack<string> &token);
int charType(char x);
void operandSeen(stack<string> &token, char x);
void operatorSeen(stack<string> &token, char x);
int calc(int a, int b, char operand);
void semicolon(stack <string> &token, char &curr, string &expression, int &position);

int main()
{
	string expression = rpnInput();
	stack <string> post;

	postfix(expression, post);

	return 0;
}

void postfix(string &expression, stack<string> &token)
{
	int len = expression.length();
	int position = 0;
	char curr = expression.at(position);
	while(curr != ';')
	{
		curr = expression.at(position);
		position++;

		int type = charType(curr);
		switch(type)
		{
			case 0:
				break;
			case 1:
				operandSeen(token, curr);
				break;
			case 2:
				operatorSeen(token, curr);
				break;
			case 3:
        			semicolon(token, curr, expression, position);
				cout << endl;
				break;
			case 4:
				cout << endl << "Invalid Expression. Now Exiting..."
				<< endl;
				curr = ';';
				break;
			default:
				break;
		}
	}
	
	//cout << len << endl; 
	//cout << position << endl;
}

void semicolon(stack <string> &token, char &curr, string &expression, int &position)
{
	if(!token.isEmpty())
	{
		cout << "Token = " << curr << "\t"
		<< "Pop "; 
		string s = token.top();
		cout << s << endl;
		token.makeEmpty();

		cout << endl << "Try Again? (Y/N) ";
		char c;
		cin >> c;
		if(toupper(c) == 'Y')
		{
			position = 0;
			expression = rpnInput();
			expression = expression.substr(1, expression.length() - 1);
			curr = expression.at(position);
		}
		else
			curr = ';';
	}
}

int calc(int a, int b, char operand)
{
	if(operand == '+')
		return a + b;
	else if(operand == '-')
		return a - b;
	else if(operand == '*')
		return a * b;
	else if(b == 0)
	{
		cout << "Cannot divide by zero." << endl;
		return 0;
	}
	else if(operand == '/')
		return a / b;
	else if(operand == '%')
		return a % b;
}

void operatorSeen(stack<string> &token, char x)
{
	cout << "Token = " << x << "\t";
	int opA =0, opB =0;
	int result;
	if(!token.isEmpty())
	{
		string temp = token.top();
		stringstream s(temp);
		s >> opB;
		token.pop();
		cout << "Pop " << opB << "\t" << "\t";
	}
	if(!token.isEmpty())
	{
		string temp = token.top();
		stringstream s(temp);
		s >> opA;
		token.pop();
		cout << "Pop " << opA << "\t" << "\t";
	}
	if( true )
	{
		result = calc(opA, opB, x);
		cout << "Push " << result << endl;
		ostringstream s;
		s << result;
		string t = s.str();
		token.push(t);
	}
}

void operandSeen(stack<string> &token, char x)
{
	if(isdigit(x))
	{
		string i(1, x);
		token.push(i);
		cout << "Token = " << x
		<< "\t" << "Push " << x << endl;
	}	
}

int charType(char x)
{
		if(x == ' ')
			return 0;
		else if(isdigit(x))
			return 1;
		else if(isOperator(x))
			return 2;
		else if(x == ';')
			return 3;
		else
			return 4;
}

string rpnInput()
{
	string uInput;

	cout << "~~~~~~~~~~~~~~~~~~ " 
	<< "THE POSTFIX EVALUATOR " 
	<< "~~~~~~~~~~~~~~~~~~" << endl;
	cout << "Your RPN Expression: ";
	getline(cin, uInput, ';');
	cout << "Expression (rpn): " << uInput << endl;
	cout << "Length is " << uInput.length() << endl;
	uInput += ';';
 
 return uInput;
}

bool isOperator(char i)
{
	if(i == '+')
		return true;
	else if(i == '-')
		return true;
	else if(i == '*')
		return true;
	else if(i == '/')
		return true;
	else if(i == '%')
		return true;
	else
		return false;
}

