#include <iostream>
#include <string>
#include "stack.h"
#include <sstream> //String stream
using namespace std;

string rpnInput();
bool isOperator(string i);
void postfix(string &expression, stack<string> &token);
int charType(string x);
void operandSeen(stack<string> &token, string x);
void operatorSeen(stack<string> &token, string x);
int calc(int a, int b, string operand);
void semicolon(stack <string> &token, string &curr, string &expression, int &position);

int main()
{
	//Expression is the string input by the user
	string expression = rpnInput();
	stack <string> post;
	//After creating a stack to store the values in the expression
	// we send it into the postfix function
	postfix(expression, post);

	return 0;
}

void postfix(string &expression, stack<string> &token)
{
	int len = expression.length();
	int position = 0;
	string curr(1, expression.at(position));
	//While a semicolon is not selected, the loop will run
	while(curr != ";")
	{
		string curr(1, expression.at(position));
		position++;
		//Returns the value type: space, operand... etc
		int type = charType(curr);
		switch(type)
		{
			//If value is a space, skip
			case 0:
				break;
			//If value is an integer check for consecutive digits, 
			// add, then call operand function
			case 1:
				if((position) < len && isdigit(expression.at(position)))
				{
					while(isdigit(expression.at(position)))
					{
						curr += expression.at(position);
						position ++;
					}
				}
				operandSeen(token, curr);
				break;
			//If value is an operator call the defined function
			case 2:
				operatorSeen(token, curr);
				break;
			//If value is a semicolon return the postfix expression, and
			// see if user wants to continue
			case 3:
        			semicolon(token, curr, expression, position);
				cout << endl;
				break;
			case 4:
				cout << endl << "Invalid Expression. Now Exiting..."
				<< endl;
				curr = ";";
				break;
			default:
				break;
		}
	}
}

void semicolon(stack <string> &token, string &curr, string &expression, int &position)
{
//This function is called at the end of a function. It takses the last token, pops it
// then empties the stack. Afterwards, we ask the user if they want to continue 
// or not
	if(!token.isEmpty())
	{
		cout << "Token = " << curr << "\t"
		<< "Pop "; 
		string s = token.top();
		cout << s << endl;
		token.makeEmpty();

		cout << endl << "Try Again? (Y/N) ";
		string c;
		cin >> c;
		if(c == "y" || c == "Y")
		{
			position = 0;
			expression = rpnInput();
			expression = expression.substr(1, expression.length() - 1);
			curr = expression.at(position);
		}
		else
			curr = ";";
	}
}

int calc(int a, int b, string operand)
{
//This function performs all math operations
	if(operand == "+")
		return a + b;
	else if(operand == "-")
		return a - b;
	else if(operand == "*")
		return a * b;
	else if(b == 0)
	{
		cout << "Cannot divide by zero." << endl;
		return 0;
	}
	else if(operand == "/")
		return a / b;
	else if(operand == "%")
		return a % b;
}

void operatorSeen(stack<string> &token, string x)
{
//Calculates and displays results when an operator is seen
	cout << "Token = " << x << "\t";
	int opA =0, opB =0;
	int result;
	if(!token.isEmpty())
	{
		string temp = token.top();
		stringstream s(temp);
		s >> opB;
		token.pop();
		cout << "Pop " << opB << "\t" << "\t";
	}
	if(!token.isEmpty())
	{
		string temp = token.top();
		stringstream s(temp);
		s >> opA;
		token.pop();
		cout << "Pop " << opA << "\t" << "\t";
	}
	if( true )
	{
		result = calc(opA, opB, x);
		cout << "Push " << result << endl;
		ostringstream s;
		s << result;
		string t = s.str();
		token.push(t);
	}
}

void operandSeen(stack<string> &token, string x)
{
//When operand is seen, it is pushed to stack
	if(x[0] == '-')
	{
		if(isdigit(x[1]))
		{
			token.push(x);
			cout << "Token = " << x
			<< "\t" << "Push " << x << endl;
		}
	}
	else if(isdigit(x[0]))
	{
		token.push(x);
		cout << "Token = " << x
		<< "\t" << "Push " << x << endl;
	}	
}

int charType(string x)
{
//Returns the different charType values
		if(isdigit(x[0]))
				return 1;
		else if(x == " ")
			return 0;
		else if(isOperator(x))
			return 2;
		else if(x == ";")
			return 3;
		else
			return 4;
}

string rpnInput()
{
//Takes the user input for an expression
	string uInput;

	cout << "~~~~~~~~~~~~~~~~~~ " 
	<< "THE POSTFIX EVALUATOR " 
	<< "~~~~~~~~~~~~~~~~~~" << endl;
	cout << "Your RPN Expression: ";
	getline(cin, uInput, ';');
	cout << "Expression (rpn): " << uInput << endl;
	cout << "Length is " << uInput.length() << endl;
	uInput += ';';
 
 return uInput;
}

bool isOperator(string i)
{
//Returns true if string sent in is an operator
	if(i == "+")
		return true;
	else if(i == "-")
		return true;
	else if(i == "*")
		return true;
	else if(i == "/")
		return true;
	else if(i == "%")
		return true;
	else
		return false;
}

