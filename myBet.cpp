#include <iostream>
#include <string>
#include "stack.h"
#include <fstream>
using namespace std;

void fileInput();
bool higherPrecedence(char top, char input);
bool isOperator(char i);
string infixToPostfix(string infix);

struct BETNode
{
	char info;
	BETNode* left;
	BETNode* right;
};
BETNode* postfixToBET(string postfix);
void preorder(const BETNode *node);

int main()
{
	fileInput();

}//End of main

string infixToPostfix(string infix)
{
  //   ***STEPS FOR INFIX -> POSTFIX*** //
	//1. Push ( onto stack
	//2. Output all operands (#s)
	//3. Check if any operator on top of stack
	//   If so, pop all those of higher or equal precedence
	//   push operator
	//4. If see ) pop all stack symbols until finding a (.
	//   Pop (. Output all symbols except (
	//5. End of input: Pop all stack symbols. Output them.
	string postfix = "";
	stack<char> hold;

	for(int i = 0; i < infix.length(); i++)
	{
		if(infix[i] == '(')	//1
		{
			hold.push(infix[i]);
		}
		else if(isdigit(infix[i]))	//2
		{
			postfix += infix[i];
		}
		if(isOperator(infix[i]))	//3
		{	
			while(higherPrecedence(hold.top(), infix[i]))
			{
				postfix += hold.topAndPop();
			}
			hold.push(infix[i]);
		}
		if(infix[i] == ')')	//4
		{
			while((hold.top() != '(') && (i < infix.length()))
			{
				postfix += hold.topAndPop();
			}
			hold.pop();
		}
	}
	while(!hold.isEmpty())	//5
	{
		postfix += hold.topAndPop();	
	}
	return postfix;
}

void preorder(const BETNode *node)
{
	//Prints a preorder (PLR) traversal of the inserted node
	if(node == NULL)
		;
	else
	{
		cout << node -> info << " ";
		preorder(node ->left); 
		preorder(node -> right);
	}
}

BETNode* postfixToBET(string postfix)
{	//Takes the postfix in string form and places it 
	// in a Binary Expression Tree BET
	stack<BETNode*> pointers;
	for(int i=0; i < postfix.length(); i++)
	{
		if(isdigit(postfix[i]))
		{
			BETNode* node = new BETNode;
			node -> info = postfix[i];
			node -> left = NULL;
			node -> right = NULL;
			pointers.push(node);
		}
		else if(isOperator(postfix[i]))
		{
			BETNode* node = new BETNode;
			node -> info = postfix[i];
			node -> right = pointers.topAndPop();
			node -> left = pointers.topAndPop();
			pointers.push(node);
		}
	}
	return pointers.topAndPop();
}

void fileInput()
{
	//While there is a line in the specified file,
	// take the input and print out the preorder traversal
	ifstream infile("infix.dat");
	string s;
	while(getline(infile, s))
	{
		preorder(postfixToBET(infixToPostfix(s)));
		cout << endl << endl;
	}
}

bool higherPrecedence(char top, char input)
{
	//Determines if the current char has higher precedence than
	// the char at the top of the stack
	if(top == input)
		return true;
	if((top == '*') || (top =='/'))
		return true;
	if((top == '+') || (top == '-'))
		if((input == '-') || (input == '+'))
			return true;
	if(top == '(')
		return false;
	return false;
}

bool isOperator(char i)
{
	//Return true if the char pointed to is
	// an operator
	if(i == '+')
		return true;
	else if(i == '-')
		return true;
	else if(i == '*')
		return true;
	else if(i == '/')
		return true;
	else if(i == '%')
		return true;
	else
		return false;
}
