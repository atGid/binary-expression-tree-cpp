# Binary Expression Tree Cpp

***Gideon Ojo
	CSC 245
	Project #2

BINARY EXPRESSION TREES

PART I:
  1. Take in the user input expression and output it at as string.
  2. While loop on the string(expression) begins and does not stop
	till a semicolon character is seen
  3. Check to see if the type of character is recognized.
	Looking for operators, and operands in particular.
  4. Send character type into a switch case that will determine what operation
	occurs on the particular value. It can be output, or pushed into a stack
  *  Create a function isOperator to determine if an operator is seen or not
  *  Actions dependent on an OperatorSeen or OperandSeen
  *  Function to calculate the actual value of operation on two integers

PART II:
  1. Take in input from a predifined file
  2. Each line is an expression in Infix form. Take it from infix for and return the 
	postfix of that expression (not including parenthesis).
  3. Send the postfix expression into another function to change it (in string form)
	to a Binary Expression Tree of type BETNode
  4. Lastly, take the BET and perform a simple preorder traversal on the tree.
	this is output to the screen.
  *  Loop this for each expression while there is still a line of characters.

